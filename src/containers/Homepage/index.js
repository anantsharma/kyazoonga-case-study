import React, {Component} from 'react';
import BannerCarousel from '../../components/Carousel';
import HotTicketSection from '../../components/HotTicketSections';
import HomeCategories from '../../components/HomeCategories';
import HomeSellerCategories from '../../components/HomeSellerCategories';
import HomeFeatured from '../../components/HomeFeatured';

import mockData from './data.json';

export default class HomePage extends Component{
  constructor() {
    super()
    this.state = {
      mockData: {
        hotTickets: []
      }
    };
  }

  componentDidMount() {
    this.setState({
      mockData: this.getMockData()
    });
  }

  getMockData = () => {
    return mockData;
  }

  render() {
    const {
      mockData: {
        hotTickets
      }
    } = this.state;

    return(
      <div>
        <BannerCarousel />
        <HotTicketSection hotTickets={hotTickets} />
        <HomeCategories />
        <HomeSellerCategories />
        <HomeFeatured />
      </div>
    )
  }
}