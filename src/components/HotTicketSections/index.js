import React, { Component } from 'react';
import PropTypes from 'prop-types';
import HotTicketCard from '../HotTicketCard'

export default class HotTicketSection extends Component {
  render() {

    const {
      hotTickets
    } = this.props;

    return (
      <section className="hot-ticket-sec ticket-boxes">
        <div className="container">
          <h2 className="site-title">Hot Tickets <small>Get tickets to your favourite events</small></h2>
          <div className="row">
            {
              hotTickets.map(ticket => <HotTicketCard ticket={ticket} />)
            }
          </div>
          <div className="btn-sec text-center p-20 pt-0">
            <a href="#" className="btn btn-primary btn-custome-lg">Discover More</a>
          </div>
        </div>
      </section>
    )
  }
}

HotTicketSection.propTypes = {
  hotTickets: PropTypes.array
}