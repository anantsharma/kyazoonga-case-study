import React, { Component } from 'react';
import NavBar from './components/Navbar'
import Homepage from './containers/Homepage'
import Footer from './components/Footer'

class App extends Component {
  render() {
    return (
      <div>
        <NavBar />
        <Homepage />
        <Footer />
      </div>      
    );
  }
}

export default App;
